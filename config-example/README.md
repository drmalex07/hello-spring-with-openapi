# README - Configuration

Copy and tweak configuration files under `src/main/resources/config/`:

* `application.properties`
* `application-*.properties`

