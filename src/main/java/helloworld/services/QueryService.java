package helloworld.services;

import helloworld.model.queries.Query;
import helloworld.model.queries.Answer;

public interface QueryService
{
    Answer query(Query q);
}
