package helloworld.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import helloworld.model.queries.Query;
import helloworld.model.queries.Answer;

@Service
public class DefaultQueryService implements QueryService
{
    @Value("${helloworld.answer-of-everything:42}")
    private Integer answer;
    
    @Override
    public Answer query(Query q)
    {
        return new Answer(answer, 0.90);
    }

}
