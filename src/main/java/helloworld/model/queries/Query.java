package helloworld.model.queries;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "Query", description = "Represents a query")
public class Query 
{
    @Schema(name = "Query.Specification", description = "A specification to be expected")
    public static class Specification 
    {
        @Schema(description = "A minimum QoS value", example = "0.5")
        final double minQos;

        @JsonCreator
        public Specification(@JsonProperty("minQos") double minQos)
        {
            this.minQos = minQos;
        }
        
        @JsonProperty("minQos")
        @DecimalMin("0.0")
        @DecimalMax("1.0")
        public double getMinQos()
        {
            return minQos;
        }
    }
    
    @Schema(description = "A question", example = "Quo vandis?")
    private String question;
    
    @Schema(description = "A specification")
    private Specification spec; 
    
    public Query() 
    {
        this.question = null;
        this.spec = new Specification(0.75);
    }
    
    public Query(String question)
    {
        Assert.isTrue(question != null && !question.isEmpty(), "expected a non-empty question");
        this.question = question;
        this.spec = new Specification(0.75);
    }
    
    @Override
    public String toString()
    {
        return String.format("Query(question='%s',spec=Spec(minQos=%.2f))", 
            this.question, this.spec.minQos);
    }

    @JsonProperty("question")
    @NotEmpty
    public String getQuestion()
    {
        return question;
    }

    @JsonProperty("question")
    public void setQuestion(String question)
    {
        this.question = question;
    }
    
    @JsonProperty("spec")
    @NotNull
    public Specification getSpec()
    {
        return spec;
    }
    
    @JsonProperty("spec")
    public void setSpec(Specification spec)
    {
        this.spec = spec;
    }
}