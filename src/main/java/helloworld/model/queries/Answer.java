package helloworld.model.queries;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "Answer", description = "The answer to a query")
public class Answer
{
    private final int answer;
    
    private final double qos; 

    @JsonCreator
    public Answer(
        @JsonProperty("answer") int answer,
        @JsonProperty("qos") double qos)
    {
        this.answer = answer;
        this.qos = qos;
    }
    
    @Schema(description = "The answer as an integer", example = "42")
    @JsonProperty("answer")
    public int getAnswer()
    {
        return answer;
    }
    
    @Schema(description = "The Quality-of-Service value", example = "0.75")
    @JsonProperty("qos")
    @DecimalMin("0.0")
    @DecimalMax("1.0")
    public double getQos()
    {
        return qos;
    }
}