package helloworld.controllers;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import helloworld.model.RestResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;

@ControllerAdvice(annotations = { RestController.class })
public class GlobalRestControllerAdvice
{
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public @ResponseBody RestResponse<Void> handleException(HttpMessageNotReadableException ex) 
    {
        RestResponse<Void> r = RestResponse.error("Cannot parse input: " + ex.getMessage());
        return r;
    }
}
