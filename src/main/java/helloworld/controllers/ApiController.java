package helloworld.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Schema;

import helloworld.model.RestResponse;
import helloworld.model.queries.Answer;
import helloworld.model.queries.Query;
import helloworld.services.QueryService;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "query-api", description = "The Query API")
public class ApiController 
{
    private static final Logger logger = LoggerFactory.getLogger(ApiController.class);
    
    @Autowired
    private QueryService queryService;
    
    /**
     * An example query endpoint.
     * 
     * @param q A query object
     */
    @Operation(summary = "Perform a query", responses = {
        @ApiResponse(responseCode = "200", description = "successful query"),
        @ApiResponse(responseCode = "400", description = "invalid query", 
            content = @Content(
                schema = @Schema(implementation = RestResponse.class),
                mediaType = MediaType.APPLICATION_JSON_VALUE,
                examples = {@ExampleObject(value = "{\"status\": \"FAILED\", \"error\": \"Something is wrong\"}")})
        )
    })    
    @PostMapping(path = "/api/query", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RestResponse<Answer> query(@RequestBody Query q) 
    {        
        logger.info("Received query: {}", q);

        Assert.notNull(q.getQuestion(), "Expected a non-empty question");
        
        Answer result = queryService.query(q); 
        
        // The returned object will be automatically serialized to JSON based on "produces" attribute
        return RestResponse.result(result);
    }
 
    /**
     * An exception handler that translates exceptions thrown by this REST controller
     * methods to an HTTP "Bad Request" response.
     * @param ex The exception object
     */
    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.BAD_REQUEST) // Note: do not use reason (ignores response body)
    public RestResponse<Void> handleException(IllegalArgumentException ex)
    {
        return RestResponse.error(ex.getMessage());
    }
}
