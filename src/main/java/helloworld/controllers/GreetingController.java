package helloworld.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    private static final Logger logger = LoggerFactory.getLogger(GreetingController.class);
    
    /**
     * An example of a member with value injected from application properties
     */
    @Value("${helloworld.greeting-message:Hello World}")
    private String greetingMessage;

    /**
     * As above, note the auto-conversion to Integer
     */ 
    @Value("${helloworld.answer-of-everything:42}")
    private Integer answer;
    
    @GetMapping({"/", "index"})
    public String home() throws Exception 
    {
        logger.info("Greeting the world...");
        
        LocalDateTime now = LocalDateTime.now();       
        return String.format(
            "<html><body>" + 
              "<h1>%s</h1><p><small>Generated at %s</small></p>" + 
            "</body></html>",
            greetingMessage,
            now.format(DateTimeFormatter.ISO_DATE_TIME)
        );
    }
    
    @GetMapping(value = "/about", produces = MediaType.TEXT_PLAIN_VALUE)
    public String about() {
        return "This is an example!";
    }
}
