package helloworld.integration.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import helloworld.model.queries.Query;

/**
 * A test suite for a RESTful API
 */
@RunWith(SpringRunner.class)
@ActiveProfiles("testing")
@SpringBootTest // Load the full application (plus any @TestConfiguration classes)
@WebAppConfiguration
public class ApiControllerTests
{
    @TestConfiguration
    static class Setup
    {
        private Random random = new Random();
        
        // Define additional beans to be used for tests
    }
    
    @Autowired
    private WebApplicationContext applicationContext;
    
    private MockMvc mockmvc;
    
    @Autowired
    private ObjectMapper jsonMapper;
        
    @Before
    public void setup() throws Exception 
    {
        // Initialize mock MVC         
        this.mockmvc = MockMvcBuilders.webAppContextSetup(this.applicationContext)
            //.apply(springSecurity()) // apply the Spring-Security filter chain
            .build();
    }
    
    //
    // Tests
    //
    
    @Test
    public void submitQuery1() throws Exception
    {
        final Query q = new Query("What is the answer to everything");
        final String payload = jsonMapper.writeValueAsString(q);
        
        MvcResult mvcresult = mockmvc
            .perform(post("/api/query")
                //.with(user(user1)) // impersonate user by UserDetails
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(payload))
            // Debug
            //.andDo(print())  
            // Assert
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status").value("SUCCESS"))
            .andExpect(jsonPath("$.error").isEmpty())
            .andExpect(jsonPath("$.result.answer").isNumber())
            .andReturn();
       
        System.err.printf("%n%n%s %s%n%s%n%nHTTP %d%n -- %n%s%n%n", 
            mvcresult.getRequest().getMethod(), mvcresult.getRequest().getServletPath(), 
            mvcresult.getRequest().getContentAsString(),
            mvcresult.getResponse().getStatus(), mvcresult.getResponse().getContentAsString());
    }
    
    @Test
    public void submitQueryEmptyQuestion() throws Exception
    {
        final Query q = new Query();
        final String payload = jsonMapper.writeValueAsString(q);
        
        MvcResult mvcresult = mockmvc
            .perform(post("/api/query")
                //.with(user(user1))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(payload))
            //.andDo(print())
            .andExpect(status().isBadRequest())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status").value("FAILURE"))
            .andExpect(jsonPath("$.result").isEmpty())
            .andExpect(jsonPath("$.error").isNotEmpty())
            .andReturn();
    }
}