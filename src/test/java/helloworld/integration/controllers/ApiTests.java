package helloworld.integration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URI;
import java.time.Duration;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import helloworld.model.EnumResponseStatus;
import helloworld.model.RestResponse;
import helloworld.model.queries.Query;
import helloworld.model.queries.Answer;

@RunWith(SpringRunner.class)
@ActiveProfiles({ "testing", "mock-authentication-provider" })
@SpringBootTest( // Load the full application
    webEnvironment = WebEnvironment.RANDOM_PORT, // Start the embedded web server
    properties = { 
        "server.servlet.context-path=/" 
    })
public class ApiTests
{
    private static Logger logger = LoggerFactory.getLogger(ApiTests.class);
    
    @TestConfiguration
    public static class Setup
    {
        @Bean("defaultRestTemplate")
        public RestTemplate defaultRestTemplate()
        {
            return new RestTemplateBuilder()
                .setConnectTimeout(Duration.ofMinutes(1))
                .build();
        }
    }
    
    @Value("${local.server.port}")
    private Integer serverPort;
    
    @Autowired
    private ObjectMapper jsonMapper;
    
    @Autowired
    @Qualifier("defaultRestTemplate")
    private RestTemplate rest;

    @Test
    public void submitQuery1() throws Exception
    {
        Query q = new Query("What is the answer to everything");
        String payload = jsonMapper.writeValueAsString(q);
        
        URI uri = UriComponentsBuilder.fromPath("/api/query")
            .scheme("http").host("localhost").port(serverPort)
            .build().toUri();
        
        System.err.printf("%n%n ** Testing POST %s %s%n%n", uri, payload);
        
        RequestEntity<Query> req = RequestEntity.post(uri)
            .header("Authorization-Token", "tester")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .body(q);
        
        ParameterizedTypeReference<RestResponse<Answer>> responseType = 
            new ParameterizedTypeReference<RestResponse<Answer>>() {};
        
        ResponseEntity<RestResponse<Answer>> res = rest.exchange(req, responseType);
            
        System.err.printf("%n%n ** Received status=%s body=%s%n%n", 
            res.getStatusCode(), jsonMapper.writeValueAsString(res.getBody()));
            
        assertEquals(HttpStatus.OK, res.getStatusCode());
        
        RestResponse<Answer> resBody = res.getBody();
        assertEquals(EnumResponseStatus.SUCCESS, resBody.getStatus());
        
        Answer result = resBody.getResult();
        assertNotNull(result);  
    }
}
