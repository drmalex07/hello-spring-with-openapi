package helloworld.unit.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import helloworld.controllers.ApiController;
import helloworld.model.RestResponse;
import helloworld.model.queries.Query;
import helloworld.model.queries.Answer;
import helloworld.model.EnumResponseStatus;
import helloworld.services.QueryService;

@RunWith(SpringRunner.class)
@ActiveProfiles("testing")
public class ApiControllerTests
{
    /** A dependency of our under-testing controller */
    @Mock
    private QueryService queryService;
    
    /**
     * This is the controller under unit testing. 
     * Mockito will try to inject any locally declared mocks into.
     */
    @InjectMocks 
    private ApiController controller;
    
    private static final int QUERY_ANSWER = 1997;
    
    @Before
    public void setup() throws Exception 
    {
        // Initialize mocks present here
        
        MockitoAnnotations.initMocks(this);
        
        // Configure queryService mock
        
        Answer result = new Answer(QUERY_ANSWER, 0.90);
        
        when(queryService.query(any(Query.class)))
            .thenReturn(result);
    }
    
    @Test
    public void submitQuery1() throws Exception 
    {
        Query q = new Query();
        q.setQuestion("What time is it?");
        
        RestResponse<Answer> response = controller.query(q);
        
        assertNotNull(response);
        assertEquals(response.getStatus(), EnumResponseStatus.SUCCESS);
        
        Answer result = response.getResult();
        assertNotNull(result);
        assertEquals(result.getAnswer(), QUERY_ANSWER);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void submitQueryEmptyQuestion() throws Exception
    {
        Query q = new Query();
        RestResponse<Answer> response = controller.query(q);
    }
    
}
