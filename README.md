# Hello Spring

A Hello-World Spring application documenting itself via OpenAPI

## Quickstart

### Build

Configure project following the examples under `config-example` folder. See [more](config-example/README.md).

Build the project:

    mvn clean package

Build documentation:

    mvn site

Run integration tests (use dedicated Maven profile to enable)

    mvn -P integration-tests verify

### Run as standalone JAR

Deploy as a standalone JAR with an embedded server (here Tomcat 8.x). Our `pom.xml` has a packaging type of `jar`.

Run a standalone JAR:

    java -jar target/hello-spring-0.0.1-SNAPSHOT-exec.jar

During development, we usually run the application with `spring-boot:run` (passing additional properties with `-Dfoo=bar`, if needed): 

    mvn spring-boot:run -Dfoo=bar

### View OpenAPI/Swagger documentation

The OpenAPI document is at `<context-path>/api-docs`.

The Swagger web UI is at `<context-path>/swagger-ui.html`
